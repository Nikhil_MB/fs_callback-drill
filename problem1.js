/*
    Problem 1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require("fs");

const deleteFile = (filePath) => {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.log(err);
    }
  });
};

const writeToFile = (fileNames, dirPath) => {
  fileNames.forEach((file) => {
    const filePath = [dirPath, "/", file].join("");
    fs.writeFile(filePath, "", (err) => {
      if (err) {
        console.log(err);
      } else {
        deleteFile(filePath);
      }
    });
  });
};

const problem1 = (fileNames, dirPath) => {
  fs.access(dirPath, (notExists) => {
    if (notExists) {
      fs.mkdir(dirPath, (err) => {
        if (err) {
          console.log(err);
        } else {
          writeToFile(fileNames, dirPath);
        }
      });
    } else {
      writeToFile(fileNames, dirPath);
    }
  });
};

module.exports = problem1;
