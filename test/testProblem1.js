const problem1 = require("../problem1");

const fileNames = [
  "file1.json",
  "file2.json",
  "file3.json",
  "file4.json",
  "file5.json",
];
const dirPath = "./Random";

problem1(fileNames, dirPath);
