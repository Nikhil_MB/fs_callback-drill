const fs = require("fs");

const readFile = (filePath, cb) => {
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      console.log(err);
    } else {
      cb(data);
    }
  });
};

const writeFile = (filePath, data) => {
  fs.writeFile(filePath, data, (err) => {
    if (err) {
      console.log(err);
    }
  });
};

const appendToFile = (filePath, data) => {
  fs.appendFile(filePath, data, "utf-8", (err) => {
    if (err) {
      console.log(err);
    }
  });
};

const deleteFiles = (filePath) => {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.log(err);
    }
  });
};

const problem2 = (dataFile) => {
  const namesFile = "filenames.txt";
  let filePath = dataFile;

  readFile(filePath, (data) => {
    filePath = "newFile1.txt";
    const upperCaseText = data.toUpperCase();
    writeFile(filePath, upperCaseText);

    let namesData = [filePath, "\n"].join("");
    appendToFile(namesFile, namesData);

    readFile(filePath, (data) => {
      filePath = "newFile2.txt";
      const lowerCaseText = data.toLowerCase();
      const txtArray = lowerCaseText.split(". ");

      txtArray.forEach((sentence) => {
        sentence = sentence.replace("\n", "");
        txtToWrite = [sentence, ".\n"].join("");
        appendToFile(filePath, txtToWrite);
      });

      namesData = [filePath, "\n"].join("");
      appendToFile(namesFile, namesData);

      readFile(filePath, (data) => {
        filePath = "newFile3.txt";
        let dataArray = data.split(".\n");
        const sortedData = dataArray.sort();

        sortedData.forEach((sentence) => {
          txtToWrite = [sentence, ".\n"].join("");
          appendToFile(filePath, txtToWrite);
        });

        namesData = [filePath, "\n"].join("");
        appendToFile(namesFile, namesData);

        readFile(namesFile, (data) => {
          dataArray = data.split("\n");

          dataArray.forEach((filePath) => {
            if (filePath) {
              deleteFiles(filePath);
            }
          });
        });
      });
    });
  });
};

module.exports = problem2;
